#!/bin/bash
echo "Start script execution---->>04-install-minikube.sh<----"
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/
minikube version
sudo apt install conntrack
sudo minikube start --vm-driver=docker --force
sudo minikube status

echo "End script execution---->>04-install-minikube.sh<<----"